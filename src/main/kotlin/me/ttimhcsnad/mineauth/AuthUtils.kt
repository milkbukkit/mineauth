package me.ttimhcsnad.mineauth

import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender

class AuthUtils {
  companion object {

    /**
     * The amount of milliseconds to allow for verification of token
     */
    private const val LENIENCY_MS = 15000

    /**
     * Helper method to send a player a colorized message
     *
     * @param player The player being sent the message
     * @param message The message being colorized and sent
     */
    fun sendMessage(player : CommandSender, message : String) {
      player.sendMessage(colorize(message))
    }

    /**
     * Helper method to send a collection of colorized messages to a player
     *
     * @param player The player being sent the messages
     * @param messages The messages being colorized and sent
     */
    fun sendMessage(player : CommandSender, messages : Collection<String>) {
      player.sendMessage(colorize(messages).toTypedArray())
    }

    /**
     * Helper method to colorize a message
     *
     * @param message The message being colorized (Uses the '&' ampersand token for colorization)
     * @return The resulting colorized message
     */
    fun colorize(message : String) : String {
      return ChatColor.translateAlternateColorCodes('&', message)
    }

    /**
     * Helper method to replace any instance of "search" with "replacement"
     *
     * @param messages The collection of strings being scanned through
     * @param search The string being replaced
     * @param replacement The string being replaced by
     */
    fun replace(messages : Collection<String>, search : String, replacement : String) : Collection<String> {
      return messages.map { msg ->
        msg.replace(search, replacement)
      }
    }

    /**
     * Helper method to colorize a collection of messages
     *
     * @param messages The messages being colorized (Uses the '&' ampersand token for colorization)
     * @return The resulting collection of colorized messages
     */
    fun colorize(messages : Collection<String>) : Collection<String> {
      return messages.map { msg -> colorize(msg) }
    }

    /**
     * Private helper method to validate a token with a provided secret
     *
     * @param token The provided token
     * @param secret The provided secret used for validation
     * @return The result of the validation
     */
    @Throws(NumberFormatException::class)
    fun verifyAuth(token : String, secret : String) : Boolean {
      return TimeBasedOneTimePasswordUtil.validateCurrentNumber(secret, token.toInt(), LENIENCY_MS)
    }

    /**
     * Helper method to generate a secret Base32 String
     * @return The resulting secret from generation
     */
    fun generateSecret() : String {
      return TimeBasedOneTimePasswordUtil.generateBase32Secret()
    }
  }
}
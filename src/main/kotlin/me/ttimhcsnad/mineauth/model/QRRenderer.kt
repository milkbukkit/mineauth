package me.ttimhcsnad.mineauth.model

import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil
import me.ttimhcsnad.mineauth.MineAuth
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.map.MapCanvas
import org.bukkit.map.MapRenderer
import org.bukkit.map.MapView
import java.awt.Image
import java.awt.image.BufferedImage
import java.net.URL
import javax.imageio.ImageIO

class QRRenderer(val plugin : MineAuth, private val key_id : String, private val secret : String) : MapRenderer() {
  private var image : BufferedImage? = null
  private var drawn : Boolean = false

  override fun render(map : MapView, canvas : MapCanvas, player : Player?) {
    if (plugin.isEnabled) {
      // Draw image if we have it
      if (image != null) {
        if (!drawn) {
          canvas.drawImage(0, 0, image)
          drawn = true
        }
      } else {
        Bukkit.getScheduler().runTaskAsynchronously(plugin) {
          // Retrieve image from secret
          val url = URL(TimeBasedOneTimePasswordUtil.qrImageUrl(key_id, secret))
          // Resize image to fit map
          this.image = resize(ImageIO.read(url), 128, 128)
        }
      }
    }
  }

  private fun resize(img : BufferedImage, height : Int, width : Int) : BufferedImage {
    val tmp = img.getScaledInstance(width, height, Image.SCALE_REPLICATE)
    val resized = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
    val g2d = resized.createGraphics()
    g2d.drawImage(tmp, 0, 0, null)
    g2d.dispose()
    return resized
  }
}
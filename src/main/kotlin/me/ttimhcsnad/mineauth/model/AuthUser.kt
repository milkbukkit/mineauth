package me.ttimhcsnad.mineauth.model

import org.bukkit.entity.Player
import java.util.*

class AuthUser(player : Player, private val secret : String) {

  private var uuid : UUID = player.uniqueId
  private var address : String = player.address.address.toString()

  /**
   * Retrieve the [UUID] from the created [AuthUser]
   * @return The [UUID] of the player used for creation of the [AuthUser]
   */
  fun getUUID() : UUID {
    return this.uuid
  }

  /**
   * Retrieve the IP address of the [AuthUser]
   * @return The IP address of the player used creation of the [AuthUser]
   */
  fun getAddress() : String {
    return this.address
  }

  /**
   * Retrieve the secret-token associated with the [AuthUser]
   * @return The secret-token value generated during the creation of the [AuthUser]
   */
  fun getSecret() : String {
    return this.secret
  }

  override fun toString() : String {
    return "AuthUser"
  }

}
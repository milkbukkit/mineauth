package me.ttimhcsnad.mineauth.listeners

import me.ttimhcsnad.mineauth.AuthUtils
import me.ttimhcsnad.mineauth.MineAuth
import org.bukkit.Bukkit
import org.bukkit.command.PluginCommand
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryDragEvent
import org.bukkit.event.player.*

class AuthListener(private val plugin : MineAuth) : Listener {

  @EventHandler(priority = EventPriority.HIGH)
  fun onLogin(event : PlayerJoinEvent) {
    // Check if user needs to authenticate
    val player = event.player
    if (!plugin.getFileManager().validateIP(player)
            || plugin.config.getBoolean("verify-always")) {
      plugin.getUserManager().setNeedsAuth(player)
    }
    this.handleNeedsAuth(player)
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onMove(event : PlayerMoveEvent) {
    if (this.handleNeedsAuth(event.player)) {
      event.to = event.from
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onCommand(event : PlayerCommandPreprocessEvent) {
    if (event.message != null) {
      val cmdInit = event.message.replaceFirst("/", "").replace(Regex(" .+"), "")
      val cmd : PluginCommand? = Bukkit.getPluginCommand(cmdInit)
      if (cmd == null || cmd.plugin != plugin) {
        if (this.handleNeedsAuth(event.player)) {
          event.isCancelled = true
        }
      }
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onMessage(event : AsyncPlayerChatEvent) {
    if (this.handleNeedsAuth(event.player)) {
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onDropItem(event : PlayerDropItemEvent) {
    if (this.handleNeedsAuth(event.player)) {
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onHotBarChange(event : PlayerItemHeldEvent) {
    if (this.handleNeedsAuth(event.player)) {
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onInventoryClick(event : InventoryClickEvent) {
    if ((event.whoClicked is Player) and this.handleNeedsAuth(event.whoClicked as Player)) {
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onInventoryDrag(event : InventoryDragEvent) {
    if ((event.whoClicked is Player) and this.handleNeedsAuth(event.whoClicked as Player)) {
      event.isCancelled = true
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onLogout(event : PlayerQuitEvent) {
    // Teleport back to last known location on logout
    if (plugin.getUserManager().needsAuth(event.player)) {
      plugin.getUserManager().authenticate(event.player)
    }
  }

  @EventHandler(priority = EventPriority.HIGH)
  fun onToggleFly(event : PlayerToggleFlightEvent) {
    event.isCancelled = this.handleNeedsAuth(event.player)
  }

  /**
   * Private helper method to verify (and message if necessary) if a player needs authentication.
   *
   * @param player The [Player] being verified
   * @return Whether the specified [Player] needs authentication
   */
  private fun handleNeedsAuth(player : Player) : Boolean {
    if (plugin.getUserManager().needsAuth(player)) {
      val messages = plugin.messages.getStringList("needs-authentication")
      AuthUtils.sendMessage(player, messages)
      return true
    }
    return false
  }

}
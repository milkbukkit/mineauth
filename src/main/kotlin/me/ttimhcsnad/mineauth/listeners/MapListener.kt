package me.ttimhcsnad.mineauth.listeners

import me.ttimhcsnad.mineauth.MineAuth
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryOpenEvent
import org.bukkit.event.player.PlayerDropItemEvent
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.event.player.PlayerQuitEvent

class MapListener(private val plugin : MineAuth) : Listener {

  @EventHandler
  fun onLogout(event : PlayerQuitEvent) {
    plugin.getMapManager().removeAuthMaps(event.player)
  }

  @EventHandler
  fun onMapOpenInInventory(event : InventoryOpenEvent) {
    plugin.getMapManager().removeAuthMaps(event.player as Player)
  }

  @EventHandler
  fun onItemDrop(event : PlayerDropItemEvent) {
    if (plugin.getMapManager().isAuthMap(event.itemDrop.itemStack)) {
      event.itemDrop.remove()
    }
  }

  @EventHandler
  fun onItemFrameInteract(event : PlayerInteractEntityEvent) {
    val player = event.player
    val itemInHand = player.inventory.itemInMainHand
    if (event.rightClicked.type == EntityType.ITEM_FRAME) {
      if (plugin.getMapManager().isAuthMap(itemInHand)) {
        event.isCancelled = true
      }
    }
  }

}
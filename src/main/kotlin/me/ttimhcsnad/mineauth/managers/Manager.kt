package me.ttimhcsnad.mineauth.managers

interface Manager {

  /**
   * Initialize a [Manager] sub-class.
   * This will setup any required fields for the sub-class during plugin initialization.
   * @return The instance of [Manager] set up. Useful for chaining calls.
   */
  fun init() : Manager
}
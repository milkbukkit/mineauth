package me.ttimhcsnad.mineauth.managers

import me.ttimhcsnad.mineauth.AuthUtils
import me.ttimhcsnad.mineauth.MineAuth
import me.ttimhcsnad.mineauth.model.QRRenderer
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.map.MapView

class MapManager(private val plugin : MineAuth) : Manager {

  private lateinit var keyID : String
  private lateinit var mapName : String
  private lateinit var mapLore : List<String>

  override fun init() : MapManager {
    keyID = plugin.config.getString("server-name") + "_auth"
    mapName = AuthUtils.colorize(plugin.config.getString("auth-map.name"))
    mapLore = AuthUtils.colorize(plugin.config.getStringList("auth-map.lore")).toList()
    return this
  }

  /**
   * Provide an Authenticator Map to a player
   *
   * @param player The player being given the map
   * @param secret The secret being used for auth generation
   */
  fun giveAuthMap(player : Player, secret : String) {
    // Generate MapView & Apply QR Renderer
    val map = Bukkit.createMap(player.world)
    this.retrieveQR(map, secret)

    // Generate map item and give to player
    val mapItem = ItemStack(Material.MAP, 1)
    val mapMeta = mapItem.itemMeta
    mapMeta.displayName = this.mapName
    mapMeta.lore = AuthUtils.replace(this.mapLore, "@secret", secret).toList()
    @Suppress("DEPRECATION") // Deprecated, future update will need MapMeta#setMapId()
    mapItem.durability = map.id
    mapItem.itemMeta = mapMeta
    player.inventory.addItem(mapItem)
  }

  /**
   * Private helper method to setup the provided [MapView] with its own [QRRenderer]
   *
   * @param map The [MapView] being used
   * @param secret The secret-token used for QR-code generation
   */
  private fun retrieveQR(map : MapView, secret : String) {
    // Clear renderers
    map.renderers.clear()
    // Set Scale
    map.scale = MapView.Scale.FARTHEST
    // Add QR Renderer
    map.addRenderer(QRRenderer(plugin, keyID, secret))
  }

  /**
   * Helper method to remove all maps from a player that match an "AuthMap"
   *
   * @param player The player whose AuthMap's are being removed
   */
  fun removeAuthMaps(player : Player) {
    player.inventory.contents.forEach { item ->
      item?.let {
        if (isAuthMap(item)) {
          player.inventory.remove(item)
        }
      }
    }
  }

  /**
   * Check if a [Player] has an AuthMap
   *
   * @param player The [Player] being checked
   * @return Whether the specified [Player] has an AuthMap item
   */
  fun hasAuthMap(player : Player) : Boolean {
    var result = false
    player.inventory.contents.forEach { item ->
      item?.let {
        if (result) return@forEach
        result = isAuthMap(item)
      }
    }
    return result
  }

  /**
   * Check if a specified [ItemStack] is an AuthMap
   *
   * @param item The item being checked
   * @return Whether the item is a valid AuthMap
   */
  fun isAuthMap(item : ItemStack) : Boolean {
    if (!item.hasItemMeta()) return false
    if (!item.itemMeta.hasDisplayName()) return false
    if (item.itemMeta.displayName != this.mapName) return false
    return true
  }

  /**
   * Helper method to remove all AuthMap's from everyone online
   */
  fun removeAllAuthMaps() {
    Bukkit.getOnlinePlayers().forEach { player ->
      this.removeAuthMaps(player)
    }
  }

}
package me.ttimhcsnad.mineauth.managers

import me.ttimhcsnad.mineauth.MineAuth
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.entity.Player
import java.util.*
import kotlin.collections.HashMap

class UserManager(private val plugin : MineAuth) : Manager {

  private lateinit var needsAuthentication : MutableList<UUID>
  private lateinit var lastLocation : MutableMap<UUID, Location>
  private lateinit var tempSecrets : MutableMap<UUID, String>

  override fun init() : UserManager {
    this.needsAuthentication = ArrayList()
    this.lastLocation = HashMap()
    this.tempSecrets = HashMap()
    return this
  }

  /**
   * Flag a [Player] as needing to authenticate.
   * This will disallow them from doing any actions until proper validation.
   * This will set the [Player]'s last known location and teleport them to the defined world spawn.
   *
   * @param player The [Player] being flagged for auth requirement
   */
  fun setNeedsAuth(player : Player) {
    this.needsAuthentication.add(player.uniqueId)
    this.setLastLocation(player)
    player.teleport(Bukkit.getWorld(plugin.config.getString("default-tp-world")).spawnLocation)
  }

  /**
   * Flag a [Player] as not needing authentication anymore.
   * This will allow the player to continue playing normally again.
   * This will teleport the [Player] to their last-known location
   *
   * @param player The [Player] being flagged as not needing authentication anymore
   */
  fun authenticate(player : Player) {
    this.needsAuthentication.remove(player.uniqueId)
    player.teleport(this.getLastLocation(player))
    this.lastLocation.remove(player.uniqueId)
  }

  /**
   * Check if a [Player] needs to be validated
   *
   * @param player The player being checked
   * @return Whether the player needs to be validated
   */
  fun needsAuth(player : Player) : Boolean {
    return this.needsAuthentication.contains(player.uniqueId)
  }

  /**
   * Private helper method to retrieve the last known location from the [Player]
   *
   * @param player The [Player] being used to retrieve the last known location
   * @return The last known location (or null if they do not have a location set)
   */
  private fun getLastLocation(player : Player) : Location? {
    return this.lastLocation[player.uniqueId]
  }

  /**
   * Private helper method to set the last known location for the [Player]
   *
   * @param player The [Player] being used to set the last known location
   */
  private fun setLastLocation(player : Player) {
    this.lastLocation[player.uniqueId] = player.location
  }

  /**
   * Store the temporary secret value while the provided [Player] sets up their app
   *
   * @param player The player being stored as a key for the secret
   */
  fun beginSetup(player : Player, secret : String) {
    this.tempSecrets[player.uniqueId] = secret
  }

  /**
   * Retrieve the stored temporary-secret value stored for the [Player]
   *
   * @return The stored temp secret (or null if they do not have one stored)
   */
  fun getTempSecret(player : Player) : String? {
    return this.tempSecrets[player.uniqueId]
  }

}
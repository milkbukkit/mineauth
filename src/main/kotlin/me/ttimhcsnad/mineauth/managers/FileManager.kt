package me.ttimhcsnad.mineauth.managers

import me.ttimhcsnad.mineauth.MineAuth
import me.ttimhcsnad.mineauth.model.AuthUser
import org.bukkit.Bukkit
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import java.io.File
import java.nio.file.Paths

class FileManager(private var plugin : MineAuth) : Manager {

  private lateinit var dataFile : File
  private lateinit var dataConfig : FileConfiguration

  override fun init() : FileManager {
    this.dataFile = Paths.get(plugin.dataFolder.path + "/data.yml").toFile()
    this.dataConfig = YamlConfiguration.loadConfiguration(this.dataFile)
    plugin.messages = YamlConfiguration.loadConfiguration(Paths.get(plugin.dataFolder.path + "/messages.yml").toFile())
    return this
  }

  /**
   * Register an [AuthUser] to the database.
   * This will update any previous registrations if a new one is made
   *
   * @param user The [AuthUser] being registered
   */
  fun registerUser(user : AuthUser) {
    val player = Bukkit.getPlayer(user.getUUID())
    var userSection = this.getConfigSection(player)
    if (userSection != null) {
      this.updateIP(player)
      this.updateSecret(player, user.getSecret())
    } else {
      userSection = this.dataConfig.createSection(user.getUUID().toString())
      userSection.set("address", user.getAddress())
      userSection.set("secret", user.getSecret())
    }
    this.dataConfig.save(this.dataFile)
  }

  /**
   * Private helper method to update the ip address of a registered [AuthUser]
   *
   * @param player The [Player] being updated
   */
  private fun updateIP(player : Player) {
    val userSection : ConfigurationSection? = this.getConfigSection(player)
    userSection?.let {
      userSection.set("address", player.address.address.toString())
    }
  }

  /**
   * Private helper method to update the secret-token of a registered [AuthUser]
   *
   * @param player The [Player] being updated
   */
  private fun updateSecret(player : Player, secret : String) {
    val userSection : ConfigurationSection? = this.getConfigSection(player)
    userSection?.let {
      userSection.set("secret", secret)
    }
  }

  /**
   * Remove a registered user from the database
   *
   * @param player The [Player] being removed
   */
  fun removeUser(player : Player) {
    this.dataConfig.set(player.uniqueId.toString(), null)
    this.dataConfig.save(this.dataFile)
  }

  /**
   * Retrieve the secret-token of a registered user in the database
   *
   * @param player The [Player] being used to access the secret token
   * @return The secret-token associated with the registered [Player]
   */
  fun getSecret(player : Player) : String? {
    return this.getConfigSection(player)?.getString("secret")
  }

  /**
   * Validate that a [Player]'s IP address matches the one stored in the database.
   *
   * @param player The [Player] being used for validation
   * @return False if validation fails, or true if validation succeeds.
   * This will also return true if they do not need to validate.
   */
  fun validateIP(player : Player) : Boolean {
    this.dataConfig = YamlConfiguration.loadConfiguration(this.dataFile)
    if (needsValidation(player)) {
      val userSection : ConfigurationSection? = this.getConfigSection(player)
      userSection?.let {
        val address = userSection.getString("address")
        return address == player.address.address.toString()
      }
    }
    return true
  }

  /**
   * Private helper method to check whether a use is registered in the database
   *
   * @param player The [Player] being checked against
   * @return Whether or not the player is registered in the database
   */
  private fun needsValidation(player : Player) : Boolean {
    return this.getConfigSection(player) != null
  }

  /**
   * Private helper method to get [ConfigurationSection] associated with the provided [Player]
   *
   * @param player The player being used to retrieve the [ConfigurationSection]
   * @return The associated [ConfigurationSection], or null if they are not in the database
   */
  private fun getConfigSection(player : Player) : ConfigurationSection? {
    return this.dataConfig.getConfigurationSection(player.uniqueId.toString())
  }
}
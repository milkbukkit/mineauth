package me.ttimhcsnad.mineauth.commands

import me.ttimhcsnad.mineauth.AuthUtils
import me.ttimhcsnad.mineauth.MineAuth
import me.ttimhcsnad.mineauth.model.AuthUser
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

class AuthCommand(private val plugin : MineAuth) : CommandExecutor, TabCompleter {

  override fun onTabComplete(sender : CommandSender?, cmd : Command?, alias : String?, args : Array<out String>?) : MutableList<String> {
    val args1 = listOf("setup", "remove", "verify", "reload")
    val argsPlayers = Bukkit.getOnlinePlayers().map { player -> player.name }
    when (args?.size) {
      2 -> return args1.filter { arg -> args[1].contains(arg) }.toMutableList()
      3 -> {
        when (args[2]) {
          "remove" -> return argsPlayers.filter { arg -> args[2].contains(arg) }.toMutableList()
        }
      }
    }
    return ArrayList()
  }

  override fun onCommand(sender : CommandSender?, cmd : Command?, lbl : String?, args : Array<out String>?) : Boolean {
    if (sender!! !is Player) {
      AuthUtils.sendMessage(sender, "You must be a player to use MineAuth commands.")
      return true
    }

    val player : Player = sender as Player

    when (args?.size) {
      1 -> { /* / auth [<token> | setup | reload ] */
        when (args[0].toLowerCase()) {
          "remove" -> invalidArgs(sender)
          "verify" -> invalidArgs(sender)
          "reload" -> this.handleReload(sender)
          "setup" -> this.handleSetup(sender, player)
          else -> this.handleAuth(sender, player, args[0])
        }
      }
      // /auth [remove|verify]
      2 -> {
        when (args[0].toLowerCase()) {
          "remove" -> this.handleRemove(sender, args[1])
          "verify" -> this.handleVerify(sender, player, args[1])
          else -> this.invalidArgs(sender)
        }
      }
      else -> this.invalidArgs(sender)
    }
    return true
  }

  /**
   * Private helper function to handle the reload command argument
   *
   * @param sender The sender performing the command
   */
  private fun handleReload(sender : CommandSender) {
    // Check permissions
    if (!sender.hasPermission("mineauth.admin")) {
      this.noPermission(sender)
      return
    }

    // Reload plugin
    plugin.reload()
    this.reloaded(sender)
  }

  /**
   * Private helper method to handle the authentication argument
   *
   * @param sender The sender performing the command
   * @param player The [Player] reference for the sender
   * @param token The token being checked for authorization
   */
  private fun handleAuth(sender : CommandSender, player : Player, token : String) {
    // Validate token
    if (plugin.getUserManager().needsAuth(player)) {
      val secret = plugin.getFileManager().getSecret(player)
      secret?.let {
        try {
          if (AuthUtils.verifyAuth(token, secret)) {
            // Authenticated Success
            plugin.getUserManager().authenticate(player)
            plugin.getFileManager().registerUser(AuthUser(player, secret))
            this.authSuccess(sender)
          } else {
            // Failed to authenticate
            this.authFailed(sender)
          }
        } catch (ex : NumberFormatException) {
          this.invalidToken(sender)
        }
      }
    } else notRequired(sender)
  }

  /**
   * Private helper method to handle the remove command argument
   *
   * @param sender The sender performing the command
   * @param playerName The name of the player being removed from the auth database
   */
  private fun handleRemove(sender : CommandSender, playerName : String) {
    if (sender.hasPermission("mineauth.remove")) {
      val p = Bukkit.getPlayer(playerName)
      if (p != null) {
        plugin.getFileManager().removeUser(p)
        this.removedUser(sender, p.name)
      } else invalidPlayer(sender, playerName)
    } else noPermission(sender)
  }

  /**
   * Private helper method to handle the verify command argument
   *
   * @param sender The sender performing the command
   * @param player The [Player] reference for the sender
   * @param token The token being used for verification
   */
  private fun handleVerify(sender : CommandSender, player : Player, token : String) {
    val secret = plugin.getUserManager().getTempSecret(player)
    secret?.let {
      try {
        if (AuthUtils.verifyAuth(token, secret)) {
          plugin.getFileManager().registerUser(AuthUser(player, secret))
          plugin.getMapManager().removeAuthMaps(player)
          this.authSuccess(sender)
        } else {
          this.authFailed(sender)
        }
      } catch (ex : NumberFormatException) {
        this.invalidToken(sender)
      }
      return
    }
    this.notRequired(player)
  }

  /**
   * Private helper method to handle the setup command argument
   *
   * @param sender The sender performing the command
   * @param player The [Player] reference for the sender
   */
  private fun handleSetup(sender : CommandSender, player : Player) {
    // Check permissions
    if (!sender.hasPermission("mineauth.use")) {
      this.noPermission(sender)
      return
    }

    // Make sure they don't already have an AuthMap in their inventory
    if (plugin.getMapManager().hasAuthMap(player)) {
      this.alreadyHaveMap(player)
      return
    }

    // Make sure they have inventory space
    if (player.inventory.firstEmpty() == -1) {
      this.noSpace(player)
      return
    }

    // Generate secret
    val secret : String = AuthUtils.generateSecret()
    plugin.getMapManager().giveAuthMap(player, secret)

    // Setup pre-registration
    plugin.getUserManager().beginSetup(player, secret)

    // Send messages
    this.tokenGenerated(sender, secret)
  }

  /**
   * Private helper method to dispatch the authentication success message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun authSuccess(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("success-authentication"))
  }

  /**
   * Private helper method to dispatch the authentication failed message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun authFailed(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("failed-authentication"))
  }

  /**
   * Private helper method to dispatch the invalid token message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun invalidToken(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.invalid-token"))
  }

  /**
   * Private helper method to dispatch the removed-playerName message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   * @param playerName The player removed from auth database
   */
  private fun removedUser(sender : CommandSender, playerName : String) {
    val messages = plugin.messages.getStringList("removed-playerName")
    AuthUtils.sendMessage(sender, AuthUtils.replace(messages, "@p", playerName))
  }

  /**
   * Private helper method to dispatch the token generated message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun tokenGenerated(sender : CommandSender, secret : String) {
    val messages = plugin.messages.getStringList("token-generated")
    AuthUtils.sendMessage(sender, AuthUtils.replace(messages, "@secret", secret))
  }

  /**
   * Private helper method to dispatch the invalid arguments message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun invalidArgs(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.invalid-args"))
  }

  /**
   * Private helper method to dispatch the invalid-player message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   * @param playerName The player name that was invalid
   */
  private fun invalidPlayer(sender : CommandSender, playerName : String) {
    val messages = plugin.messages.getStringList("errors.invalid-player")
    AuthUtils.sendMessage(sender, AuthUtils.replace(messages, "@p", playerName))
  }

  /**
   * Private helper method to dispatch the no permission message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun noPermission(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.no-permission"))
  }

  /**
   * Private helper method to dispatch the auth not required message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun notRequired(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.auth-not-required"))
  }

  /**
   * Private helper method to dispatch the have-map message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun alreadyHaveMap(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.have-map"))
  }

  /**
   * Private helper method to dispatch the no inventory space message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun noSpace(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("errors.no-space"))
  }

  /**
   * Private helper method to dispatch the reload message(s) to the requested sender
   *
   * @param sender The sender being sent the message(s)
   */
  private fun reloaded(sender : CommandSender) {
    AuthUtils.sendMessage(sender, plugin.messages.getStringList("reloaded"))
  }

}
package me.ttimhcsnad.mineauth

import me.ttimhcsnad.mineauth.commands.AuthCommand
import me.ttimhcsnad.mineauth.listeners.AuthListener
import me.ttimhcsnad.mineauth.listeners.MapListener
import me.ttimhcsnad.mineauth.managers.FileManager
import me.ttimhcsnad.mineauth.managers.MapManager
import me.ttimhcsnad.mineauth.managers.UserManager
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin
import java.nio.file.Files
import java.nio.file.Paths

class MineAuth : JavaPlugin() {

  private lateinit var userManager : UserManager
  private lateinit var fileManager : FileManager
  private lateinit var authManager : MapManager
  lateinit var messages : YamlConfiguration

  override fun onEnable() {
    this.setupFiles()
    this.setupManagers()
    this.setupListeners()
    this.setupCommands()
    logger.info("Enabled!")
  }

  override fun onDisable() {
    authManager.removeAllAuthMaps()
    logger.info("Disabled!")
  }

  /**
   * Reload the MineAuth plugin.
   * This will reload the plugin's config files
   */
  fun reload() {
    this.reloadConfig()
    this.fileManager = fileManager.init()
  }

  /**
   * Private helper method for setting up the required application-tier components
   */
  private fun setupManagers() {
    userManager = UserManager(this).init()
    fileManager = FileManager(this).init()
    authManager = MapManager(this).init()
  }

  /**
   * Private helper method for registering the [org.bukkit.event.Listener]s used in the plugin
   */
  private fun setupListeners() {
    server.pluginManager.registerEvents(AuthListener(this), this)
    server.pluginManager.registerEvents(MapListener(this), this)
  }

  /**
   * Private helper method used for establishing the executors of the plugin's command(s)
   */
  private fun setupCommands() {
    val cmd = server.getPluginCommand("auth")
    val cmdHandler = AuthCommand(this)
    cmd.executor = cmdHandler
    cmd.tabCompleter = cmdHandler
  }

  /**
   * Private helper method used for saving default resources to the plugin directory
   */
  private fun setupFiles() {
    if (!Files.exists(Paths.get(dataFolder.path + "/config.yml"))) {
      saveResource("config.yml", false)
    }
    if (!Files.exists(Paths.get(dataFolder.path + "/data.yml"))) {
      saveResource("data.yml", false)
    }
    if (!Files.exists(Paths.get(dataFolder.path + "/messages.yml"))) {
      saveResource("messages.yml", false)
    }
  }

  /**
   * Retrieve the current instance of the [UserManager] application tier component
   * @return The current instance of the [UserManager]
   */
  fun getUserManager() : UserManager {
    return userManager
  }

  /**
   * Retrieve the current instance of the [FileManager] application tier component
   * @return The current instance of the [FileManager]
   */
  fun getFileManager() : FileManager {
    return fileManager
  }

  /**
   * Retrieve the current instance of the [MapManager] application tier component
   * @return The current instance of the [MapManager]
   */
  fun getMapManager() : MapManager {
    return authManager
  }

}